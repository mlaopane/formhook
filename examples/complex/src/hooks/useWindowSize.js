import React from 'react';

function getSize() {
    return {
        innerWidth: window.innerWidth,
        outerWidth: window.outerWidth,
    };
}

export default function useWindowSize() {
    let [windowSize, setWindowSize] = React.useState(getSize());

    function handleResize() {
        setWindowSize(getSize());
    }

    React.useEffect(() => {
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    return windowSize;
}
