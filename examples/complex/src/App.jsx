import React from 'react';
import { Card } from '@mlaopane/formhook';
import './App.css';
import ComplexForm from './components/ComplexForm';
import Logger from './components/Logger';

const App = () => {
    const [formState, setFormState] = React.useState({});

    function hookContext({ form }) {
        if (JSON.stringify(form) !== JSON.stringify(formState)) {
            setFormState(form);
        }
    }

    return (
        <div id='app'>
            <Card>
                <h2 style={{ textAlign: 'center', margin: '8px 0px 24px' }}>
                    Complex form
                </h2>
                <ComplexForm hookContext={hookContext} />
                <Logger data={formState} />
            </Card>
        </div>
    );
};
export default App;
