async function validateClientSide({ field }) {
    if (!field.value) {
        return 'E-mail is mandatory';
    }
    if (!field.value.includes('@')) {
        return 'Invalid e-mail';
    }
    return '';
}

async function validateServerSide({ field }) {
    console.log('CALL check email');
    if (field.value.includes('exist')) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('The email already exists');
            }, 1000);
        });
    }

    return '';
}

async function validateClientSideThenServerSide({ field }) {
    const clientError = await validateClientSide({ field });

    if (clientError) {
        return clientError;
    }

    return await validateServerSide({ field });
}

const validateOnBlur = validateClientSideThenServerSide;
const validateOnChange = validateClientSide;
const validateOnSubmit = validateClientSideThenServerSide;

const emailValidator = {
    validate: validateOnSubmit,
    validateOnChange,
    validateOnBlur,
};

export default emailValidator;
