export const validate = async ({ field }) => {
    if (!/^(male|female)$/.test(field.value)) {
        return 'Gender is mandatory';
    }

    return '';
};

export default {
    validate,
};
