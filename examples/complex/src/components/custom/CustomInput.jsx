import React from 'react';
import './CustomInput.css';

export default function CustomInput(props) {
    return <input {...props} className='custom-input' />;
}
