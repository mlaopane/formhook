import React from 'react';

export default function CustomRadioLabel(props) {
    return (
        <label
            {...props}
            style={{ color: 'hsl(280, 80%, 40%)', marginRight: '20px' }}
        />
    );
}
