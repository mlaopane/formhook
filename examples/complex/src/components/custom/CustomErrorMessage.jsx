import React from 'react';

const color = {
    default: 'hsl(355, 50%, 50%)',
    silent: 'hsl(35, 60%, 40%)',
};

function resolveColorFromStatus(status) {
    return color[status] || color.default;
}

export default function CustomErrorMessage({ status, ...props }) {
    const style = {
        color: `${resolveColorFromStatus(status)}`,
        margin: '0.75rem 0',
    };

    return (
        <div className='custom-error-message' style={style} {...props}></div>
    );
}
