import React from 'react';
import './CustomRadioButton.css';

export default function CustomRadioButton({ checked, ...props }) {
    return (
        <div
            {...props}
            className={`custom-radio-button ${checked ? 'checked' : ''}`}
        />
    );
}
