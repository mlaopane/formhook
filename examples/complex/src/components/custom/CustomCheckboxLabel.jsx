import React from 'react';

export default function CustomCheckboxLabel(props) {
    return <div {...props} style={{ color: 'hsl(353, 85%, 30%)' }} />;
}
