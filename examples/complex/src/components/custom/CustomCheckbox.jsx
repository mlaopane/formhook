import React from 'react';
import './CustomCheckbox.css';

export default function CustomCheckbox({ checked, ...props }) {
    return (
        <div
            {...props}
            className={`custom-checkbox ${checked ? 'checked' : ''}`}
        />
    );
}
