import React from 'react';
import './Logger.css';

export default function Logger({ data }) {
    const [hidden, setHidden] = React.useState(true);

    function toggle() {
        setHidden(!hidden);
    }

    return (
        <div className={`logger ${hidden ? 'hidden' : ''}`} onClick={toggle}>
            <pre>{JSON.stringify(data, null, 4)}</pre>
        </div>
    );
}
