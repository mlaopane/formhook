import React from 'react';
import { Spy } from '@mlaopane/formhook';
import Logger from './Logger';

export default function ContextLogger() {
    const [formState, setFormState] = React.useState({});

    function hookContext({ form }) {
        setFormState(form);
    }

    return (
        <>
            <Spy hookContext={hookContext} />
            <Logger data={formState} />
        </>
    );
}
