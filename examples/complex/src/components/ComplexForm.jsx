import React from 'react';

import {
    Button,
    CheckboxGroup,
    Email,
    ErrorMessage,
    FormField,
    Form,
    Label,
    Password,
    RadioButtonGroup,
    Spy,
} from '@mlaopane/formhook';
import {
    emailValidator,
    genderValidator,
    passwordValidator,
    passwordConfirmationValidator,
} from '../validators';
import {
    CustomErrorMessage,
    CustomInput,
    CustomRadioButton,
    CustomRadioLabel,
    CustomCheckbox,
    CustomCheckboxLabel,
} from './custom';

function simulateHttpCall({ values }) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({ message: 'Submission success !' });
        }, 2000);
    });
}

async function handleSubmit({ form, dispatch }) {
    const { values } = form;
    const response = await simulateHttpCall({ values });
    console.log(response.message, { values });
    dispatch({ type: 'END_SUBMIT' });
}

export default function ComplexForm({ hookContext, ...props }) {
    const initialValues = {
        email: '',
        gender: '',
        nickname: '',
        password: '',
        passwordConfirmation: '',
        conditions: false,
    };

    const radioItems = [
        {
            id: 'female',
            label: 'Female',
        },
        {
            id: 'male',
            label: 'Male',
        },
    ];

    return (
        <Form
            id='complex-form'
            initialValues={initialValues}
            onSubmit={handleSubmit}
            {...props}
        >
            {/* Gender */}
            <FormField>
                <div style={{ display: 'flex' }}>
                    <RadioButtonGroup
                        items={radioItems}
                        name='gender'
                        radioComponent={CustomRadioButton}
                        labelComponent={CustomRadioLabel}
                        {...genderValidator}
                    />
                    <RadioButtonGroup name='gender' id='male' label='Male' />
                </div>
                <ErrorMessage name='gender' />
            </FormField>

            {/* E-mail */}
            <FormField>
                <Label style={{ fontSize: 16 }}>E-mail</Label>
                <Email
                    id='signup-email'
                    component={CustomInput}
                    {...emailValidator}
                />
                <ErrorMessage name='email' component={CustomErrorMessage} />
            </FormField>

            {/* Password */}
            <FormField>
                <Label style={{ fontSize: 16 }}>Password</Label>
                <Password
                    id='signup-password'
                    component={CustomInput}
                    {...passwordValidator}
                />
                <ErrorMessage name='password' />
            </FormField>

            {/* Password confirmation */}
            <FormField>
                <Label style={{ fontSize: 16 }}>Password confirmation</Label>
                <Password
                    id='signup-password-confirmation'
                    name={'passwordConfirmation'}
                    component={CustomInput}
                    validate={passwordConfirmationValidator.validate(
                        'password',
                    )}
                />
                <ErrorMessage name='passwordConfirmation' />
            </FormField>

            {/* Conditions */}
            <FormField>
                <CheckboxGroup
                    id='signup-conditions'
                    name={'conditions'}
                    label='I agree with the terms'
                    checkboxComponent={CustomCheckbox}
                    labelComponent={CustomCheckboxLabel}
                    validate={async ({ field }) => {
                        return !field.value
                            ? 'You must agree with the non-existing terms'
                            : '';
                    }}
                />
                <ErrorMessage name='conditions' />
            </FormField>

            {/* Submit */}
            <Button
                id='submit-button'
                type='submit'
                action='submit'
                disabled={(form) => form.isSubmitting}
            >
                Sign up
            </Button>

            <Spy hookContext={hookContext} />
        </Form>
    );
}
