import React from 'react';
import {
    Button,
    FormField,
    Form,
    Input,
    Select,
    Selector,
    Spy,
    Label,
    ErrorMessage,
} from '@mlaopane/formhook';
import { validate as validateNickname } from '../validators/nickname.validator';
import { validate as validateRace } from '../validators/race.validator';
import { validate as validateElements } from '../validators/elements.validator';
import ContextLogger from './ContextLogger';

export default function RegisterForm() {
    const [formState, setFormState] = React.useState({});

    function hookContext({ form }) {
        setFormState(form);
    }

    function handleSubmit({ form, dispatch }) {
        setTimeout(() => {
            dispatch({ type: 'END_SUBMIT' });
        }, 2000);
    }

    async function getRaces() {
        return [
            { label: 'Mage', value: 'mage' },
            { label: 'Warrior', value: 'warrior' },
        ];
    }

    async function getElements() {
        return [
            { label: 'Fire', value: 'fire' },
            { label: 'Water', value: 'water' },
            { label: 'Wind', value: 'wind' },
        ];
    }

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'space-evenly',
            }}
        >
            <Form
                initialValues={{ nickname: '', race: '', elements: [] }}
                onSubmit={handleSubmit}
            >
                <div style={{ width: '400px' }}>
                    <FormField>
                        <Label>Nickname</Label>
                        <Input
                            id='register-nickname'
                            name='nickname'
                            validate={validateNickname}
                        />
                        <ErrorMessage name='nickname' />
                    </FormField>
                    <FormField>
                        <Label>Race</Label>
                        <Select
                            id='register-race'
                            name='race'
                            defaultLabel='Select a race...'
                            component={Selector}
                            getInitialOptions={getRaces}
                            validate={validateRace}
                        />
                        <ErrorMessage name='race' />
                    </FormField>
                    <FormField>
                        <Label>Elements</Label>
                        <Select
                            id='register-elements'
                            name='elements'
                            defaultLabel='Select your elements...'
                            component={Selector}
                            getInitialOptions={getElements}
                            multiple={true}
                            validate={validateElements}
                        />
                        <ErrorMessage name='elements' />
                    </FormField>
                    <Button>Submit</Button>
                    {formState.isSubmitting && (
                        <div style={{ textAlign: 'center' }}>
                            Submit in progress...
                        </div>
                    )}
                </div>
                <div>
                    <ContextLogger />
                </div>
                <Spy hookContext={hookContext} />
            </Form>
        </div>
    );
}
