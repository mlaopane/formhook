import React from 'react';
import './ContextLogger.css';
import { Spy } from '@mlaopane/formhook';

export default function ContextLogger() {
    const [formState, setFormState] = React.useState({});
    const [hidden, setHidden] = React.useState(true);

    function hookContext({ form }) {
        setFormState(form);
    }

    function toggle() {
        setHidden(!hidden);
    }

    return (
        <>
            <Spy hookContext={hookContext} />
            <div
                className={`context-logger ${hidden ? 'hidden' : ''}`}
                onClick={toggle}
            >
                <pre>{JSON.stringify(formState, null, 4)}</pre>
            </div>
        </>
    );
}
