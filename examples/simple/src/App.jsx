import React from 'react';
import './App.css';
import RegisterForm from './components/RegisterForm';

const App = () => {
    return (
        <div id='app'>
            <RegisterForm />
        </div>
    );
};
export default App;
