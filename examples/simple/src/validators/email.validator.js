export const validate = async ({ field }) => {
    if (!field.value) {
        return 'E-mail is mandatory';
    }
    if (!field.value.includes('@')) {
        return 'Invalid e-mail';
    }
    return '';
};

export const validateOnChange = validate;

export const validateOnBlur = async ({ field }) => {
    const error = await validate({ field });

    if (error.length > 0) {
        return error;
    }

    if (field.value.includes('exist')) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('The email already exists');
            }, 1000);
        });
    }

    return '';
};

export default {
    validate,
    validateOnChange,
    validateOnBlur,
};
