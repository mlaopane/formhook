const validateForm = ({ form, dispatch }) => {
    const { values, validators } = form;
    const fieldNames = Object.keys(validators);

    const promises = fieldNames.map(async (name) => {
        const validate = validators[name] && validators[name].validate;
        const value = values[name];

        if (validate) {
            const field = { name, value };
            const error = await validate({
                field,
                form,
            });

            if (error) {
                dispatch({
                    type: 'SET_FIELD_ERROR',
                    field: { name },
                    error,
                });
                return { [name]: error };
            }
        }

        dispatch({
            type: 'CLEAR_FIELD_ERROR',
            field: { name },
        });

        return { [name]: '' };
    });

    return Promise.all(promises);
};

export default { validateForm };
