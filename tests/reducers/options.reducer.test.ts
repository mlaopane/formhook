import { FormState } from 'types';
import { formReducer } from 'reducers';
import { SET_FIELD_OPTIONS } from 'reducers/actions';
import baseState from './baseState';

describe(`Dispatching ${SET_FIELD_OPTIONS}`, function() {
    test(`should set the field options for the given field`, function() {
        const oldState: FormState = {
            ...baseState,
            options: {},
        };
        const newState = formReducer(oldState, {
            type: SET_FIELD_OPTIONS,
            payload: {
                field: {
                    name: 'city',
                },
                options: [
                    { label: 'Annecy', value: 'Annecy' },
                    { label: 'Ouagadougou', value: 'Ouagadougou' },
                ],
            },
        });

        expect(newState.options['city']).toEqual([
            { label: 'Annecy', value: 'Annecy' },
            { label: 'Ouagadougou', value: 'Ouagadougou' },
        ]);
    });
});
