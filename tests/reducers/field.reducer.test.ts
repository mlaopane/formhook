import { FormState } from 'types';
import { formReducer } from 'reducers';
import { CHANGE_FIELD, FOCUS_FIELD, BLUR_FIELD } from 'reducers/actions';
import baseState from './baseState';

describe(`Dispatching ${CHANGE_FIELD}`, function() {
    test(`should update the given field`, function() {
        const oldState: FormState = {
            ...baseState,
            values: {
                ...baseState.values,
                email: '',
                password: '',
            },
        };
        const newState = formReducer(oldState, {
            type: CHANGE_FIELD,
            payload: {
                field: {
                    name: 'email',
                    value: 'gerard@nakeur.com',
                },
            },
        });
        const formValuesEntries = Object.entries(newState.values);

        expect(formValuesEntries).toContainEqual([
            'email',
            'gerard@nakeur.com',
        ]);
        expect(formValuesEntries).toContainEqual(['password', '']);
    });
});

describe(`Dispatching ${FOCUS_FIELD}`, function() {
    test(`should focus the given field`, function() {
        const oldState: FormState = {
            ...baseState,
            focused: '',
        };
        const newState = formReducer(oldState, {
            type: FOCUS_FIELD,
            payload: {
                field: {
                    name: 'email',
                },
            },
        });

        expect(newState.focused).toBe('email');
    });
});

describe(`Dispatching ${BLUR_FIELD}`, function() {
    test(`should reset the 'focused' state to an empty string`, function() {
        const oldState: FormState = {
            ...baseState,
            focused: 'email',
        };
        const newState = formReducer(oldState, {
            type: BLUR_FIELD,
            payload: {
                field: {
                    name: 'email',
                },
            },
        });

        expect(newState.focused).toBe('');
    });
});
