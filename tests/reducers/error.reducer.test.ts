import { FormState } from 'types';
import { formReducer } from 'reducers';
import { SET_FIELD_ERROR, CLEAR_FIELD_ERROR } from 'reducers/actions';
import baseState from './baseState';

describe(`Dispatching ${SET_FIELD_ERROR}`, function() {
    test(`should set the field error for the given field`, function() {
        const oldState: FormState = {
            ...baseState,
            errors: {},
        };
        const newState = formReducer(oldState, {
            type: SET_FIELD_ERROR,
            payload: {
                field: {
                    name: 'city',
                },
                error: 'You have failed this city !',
            },
        });

        expect(newState.errors['city']).toBe('You have failed this city !');
    });
});

describe(`Dispatching ${CLEAR_FIELD_ERROR}`, function() {
    test(`should clear the field error for the given field`, function() {
        const oldState: FormState = {
            ...baseState,
            errors: {
                city: 'You have failed this city !',
            },
        };
        const newState = formReducer(oldState, {
            type: CLEAR_FIELD_ERROR,
            payload: {
                field: {
                    name: 'city',
                },
            },
        });

        expect(newState.errors['city']).toBe('');
    });
});
