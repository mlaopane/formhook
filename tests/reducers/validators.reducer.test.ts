import { FormState } from 'types';
import { formReducer } from 'reducers';
import { SET_FIELD_VALIDATORS } from 'reducers/actions';
import baseState from './baseState';

describe(`Dispatching ${SET_FIELD_VALIDATORS}`, function() {
    test(`should set the validators for the given field`, async function() {
        const oldState: FormState = {
            ...baseState,
            validators: {},
        };

        const newState = formReducer(oldState, {
            type: SET_FIELD_VALIDATORS,
            payload: {
                field: { name: 'city' },
                validate: async function() {
                    return '';
                },
            },
        });

        const cityValidator = newState.validators['city'];

        expect(typeof cityValidator.validate).toBe('function');

        const error = await cityValidator.validate({
            field: { name: 'city' },
            form: newState,
        });

        expect(error).toBe('');
    });
});
