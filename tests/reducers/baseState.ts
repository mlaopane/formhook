export default {
    errors: {},
    focused: '',
    isSubmitting: false,
    isValid: false,
    options: {},
    validators: {},
    values: {},
    touched: {},
};
