import 'jest';
import { splice } from 'helpers/array.helper';

test('splice [0, 1, 2] at index 0 should return [1, 2]', function() {
    expect(splice([0, 1, 2], 0)).toEqual([1, 2]);
});

test('splice [0, 1, 2] at index 1 should return [0, 2]', function() {
    expect(splice([0, 1, 2], 1)).toEqual([0, 2]);
});

test('splice [0, 1, 2] at index 2 should return [0, 1]', function() {
    expect(splice([0, 1, 2], 2)).toEqual([0, 1]);
});

test('splice [0, 1, 2] at an outside index should return [0, 1, 2]', function() {
    expect(splice([0, 1, 2], 10)).toEqual([0, 1, 2]);
});

test('splice [0, 1, 2] at a negative index should throw an error', function() {
    expect(() => {
        splice([0, 1, 2], -1);
    }).toThrow();
});
