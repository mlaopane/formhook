# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

> The change log has been introduced starting from `v1.7.0`

## v1.9

Release date : **2019-08-06**

### Changed

- The `Field` component has been renamed into `FormField` to prevent conflict with the `Field` type
- Internal configuration

### Fixed

- `Input` component props types
- `Input` components props types
- `RadioButtonGroup` and `CheckboxGroup` props types
- Form props type
- Change the return type form the `onSubmit` handler of the `Form` component.
- Removed `className` from the Checkbox `LabelComponent`
- Allow custom props in `ErrorMessage`
- When using a custom `ErrorMessage` component, the `status` is now passed as a prop
- `onSubmit` props is no longer required by the `Form` component
- `form.isValid` is now updated (based on the form errors)
- `Button` props type

## v1.8

Release date : **2019-08-04**

### Changed

- The `RadioButtonGroup` structure has changed : the `RadioButton` is now a child of the `RadioButtonLabel`

### Fixed

- Handle `RadioButtonGroup` validation on change event
- Handle validation on submit event specifically

## v1.7

Release date : **2019-07-15**

### Changed

- Add the `items` parameter to the `RadioButtonGroup`
- Allow validation on to the `RadioButtonGroup` by using `AsyncValidationFunction`
- Remove the `label` and `id` parameters from the `RadioButtonGroup`
