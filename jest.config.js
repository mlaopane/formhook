module.exports = {
    collectCoverageFrom: ['src/**/*.{js,ts}', '!src/context/*.{js,ts}'],
    moduleDirectories: ['node_modules', 'src'],
    preset: 'ts-jest',
    rootDir: '.',
    setupFiles: ['<rootDir>/tests/setupTests.ts'],
    testMatch: ['**/tests/**/?(*.)+(spec|test).[tj]s?(x)'],
    verbose: true,
};
