import React from 'react';
import FormContext from '../../context/FormContext';
import { FormState } from 'types';
import Dispatch from 'types/Dispatch';

export type HookContextFn = ({
    form,
    dispatch,
}: {
    form: FormState;
    dispatch: Dispatch;
}) => void;

type SpyProps = {
    hookContext: HookContextFn;
};

export default function Spy({ hookContext }: SpyProps) {
    const [form, dispatch] = React.useContext(FormContext);

    React.useEffect(() => {
        hookContext({ form, dispatch });
    }, [form, dispatch]);

    return null;
}
