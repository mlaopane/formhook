import React from 'react';
import { useSelector } from '../../hooks';
import SelectorSelection from './selection/SelectorSelection';
import SelectorOptions from './option/SelectorOptions';
import { Option } from '../../types';
import './Selector.scss';

type SelectorProps = {
    options: Option[];
    name: string;
    multiple: boolean;
    defaultLabel: string;
};

const Selector = ({
    options,
    name,
    multiple = false,
    defaultLabel = 'Select an option',
    ...props
}: SelectorProps) => {
    const {
        selection = { label: defaultLabel },
        isOpen,
        wrapperRef,
        selectOption,
        toggleSelect,
    } = useSelector({
        name,
        multiple,
    });

    return (
        <div ref={wrapperRef} {...props} className='form-selector'>
            <SelectorSelection
                multiple={multiple}
                isOpen={isOpen}
                onClick={toggleSelect}
                defaultLabel={defaultLabel}
                selection={selection}
            />
            {isOpen && (
                <SelectorOptions
                    onSelect={selectOption}
                    name={name}
                    options={options}
                    multiple={multiple}
                />
            )}
        </div>
    );
};

export default Selector;
