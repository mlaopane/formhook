import React from 'react';
import SingleSelection from './SingleSelection';
import MultipleSelection from './MultipleSelection';
import { Option } from '../../../types';
import './SelectorSelection.scss';

type SelectorSelectionProps = {
    defaultLabel: string;
    isOpen: boolean;
    multiple: boolean;
    onClick: React.MouseEventHandler;
    selection: Option | Option[];
};

const SelectorSelection = ({
    onClick,
    selection,
    isOpen = false,
    defaultLabel = 'Select an option...',
    multiple = false,
}: SelectorSelectionProps) => {
    return (
        <div
            className={`form-selector-selection${isOpen ? ' opened' : ''}`}
            onClick={onClick}
        >
            {!multiple && !Array.isArray(selection) && (
                <SingleSelection
                    defaultLabel={defaultLabel}
                    selectedOption={selection}
                />
            )}
            {multiple && Array.isArray(selection) && (
                <MultipleSelection
                    defaultLabel={defaultLabel}
                    selectedOptions={selection}
                />
            )}
        </div>
    );
};

export default SelectorSelection;
