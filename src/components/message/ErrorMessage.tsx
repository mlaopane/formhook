import React from 'react';
import { useError } from '../../hooks';
import './ErrorMessage.scss';

type Props = {
    name: string;
    component?: React.FunctionComponent<any>;
    [key: string]: any;
};

const ErrorMessage = ({
    name,
    component: CustomErrorMessage,
    ...props
}: Props) => {
    const [error, focused] = useError(name);
    const status = focused !== name ? 'default' : 'silent';

    if (CustomErrorMessage) {
        return (
            <CustomErrorMessage status={status} {...props}>
                {error}
            </CustomErrorMessage>
        );
    }

    return (
        <div className={`error-message ${status}`} {...props}>
            {error}
        </div>
    );
};

export default ErrorMessage;
