import React from 'react';
import Input, { InputProps } from './Input';

const Email = ({
    name = 'email',
    id = name,
    autoComplete = 'email',
    ...props
}: InputProps) => {
    return (
        <Input
            type='email'
            id={id}
            name={name}
            autoComplete={autoComplete}
            {...props}
        />
    );
};

export default Email;
