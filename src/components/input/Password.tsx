import React from 'react';
import Input, { InputProps } from './Input';

const Password = ({
    name = 'password',
    id = name,
    autoComplete = 'password',
    ...props
}: InputProps) => {
    return (
        <Input
            type='password'
            id={id}
            name={name}
            autoComplete={autoComplete}
            {...props}
        />
    );
};

export default Password;
