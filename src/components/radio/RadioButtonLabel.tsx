import React from 'react';
import './RadioButtonLabel.scss';
import { useRadio } from '../../hooks';
import { AsyncValidationFunction } from '../../types';

type RadioButtonLabelProps = {
    id: string;
    name: string;
    component?: any;
    validate?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnSubmit?: AsyncValidationFunction;
};

const RadioButtonLabel = ({
    children,
    id,
    name,
    component: Component = undefined,
    validate,
    validateOnChange,
    validateOnSubmit,
    ...props
}: React.PropsWithChildren<RadioButtonLabelProps>) => {
    const [, selectRadio] = useRadio({
        name,
        id,
        validate,
        validateOnChange,
        validateOnSubmit,
    });

    const LabelComponent = Component ? Component : 'label';

    return (
        <LabelComponent
            className='form-radio-label'
            onClick={selectRadio}
            htmlFor={id}
            {...props}
        >
            {children}
        </LabelComponent>
    );
};

export default RadioButtonLabel;
