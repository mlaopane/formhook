import React from 'react';
import RadioButton from './RadioButton';
import RadioButtonLabel from './RadioButtonLabel';
import './RadioButtonGroup.scss';

type RadioItem = {
    id: string;
    label: string;
};

type RadioButtonGroupProps = {
    name: string;
    id?: string;
    items: RadioItem[];
    radioComponent?: any;
    labelComponent?: any;
    [key: string]: any;
};

const RadioButtonGroup = ({
    name,
    id = name,
    items: radioItems = [],
    radioComponent = undefined,
    labelComponent = undefined,
    ...props
}: RadioButtonGroupProps) => {
    return (
        <div className='form-radio-group'>
            {radioItems.map(({ id, label }) => (
                <React.Fragment key={id}>
                    <RadioButtonLabel
                        component={labelComponent}
                        id={id}
                        name={name}
                        {...props}
                    >
                        <RadioButton
                            component={radioComponent}
                            name={name}
                            label={label}
                            id={id}
                            {...props}
                        />
                        <div>{label}</div>
                    </RadioButtonLabel>
                </React.Fragment>
            ))}
        </div>
    );
};

export default RadioButtonGroup;
