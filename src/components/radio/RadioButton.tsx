import React from 'react';
import { useRadio } from '../../hooks';
import { AsyncValidationFunction } from '../../types';
import Radio from './Radio';
import './RadioButton.scss';

type RadioButtonProps = {
    name: string;
    id: string;
    label: string;
    autoComplete?: string;
    component?: React.FunctionComponent<any>;
    validate?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnSubmit?: AsyncValidationFunction;
};

const RadioButton = ({
    name,
    id = name,
    label = '',
    autoComplete = 'off',
    component: CustomRadio = undefined,
    validate = undefined,
    validateOnChange = undefined,
    validateOnSubmit = undefined,
    ...props
}: RadioButtonProps) => {
    const [value, selectRadio] = useRadio({
        name,
        id,
        validate,
        validateOnSubmit,
        validateOnChange,
    });

    const RadioComponent = CustomRadio ? CustomRadio : Radio;

    return (
        <div className='form-radio-button' {...props}>
            <input
                className='form-input'
                type='radio'
                name={name}
                id={id}
                value={id}
                {...props}
            />
            <RadioComponent checked={value === id} onClick={selectRadio} />
        </div>
    );
};

export default RadioButton;
