import React from 'react';
import { useCheckbox } from '../../hooks';

type CheckboxLabelProps = {
    name: string;
    id: string;
    component?: any;
};

const CheckboxLabel = ({
    children,
    id,
    name,
    component: Component = undefined,
    ...props
}: React.PropsWithChildren<CheckboxLabelProps>) => {
    const [toggleCheckbox] = useCheckbox();

    const onClick = (event) => {
        event.preventDefault();
        toggleCheckbox({ name });
    };

    const LabelComponent = Component ? Component : 'label';

    return (
        <LabelComponent onClick={onClick} htmlFor={id} {...props}>
            {children}
        </LabelComponent>
    );
};

export default CheckboxLabel;
