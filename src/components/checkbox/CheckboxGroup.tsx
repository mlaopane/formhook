import React from 'react';
import Checkbox from './Checkbox';
import CheckboxLabel from './CheckboxLabel';
import './CheckboxGroup.scss';

type CheckboxGroupProps = {
    name: string;
    label?: string;
    id?: string;
    checkboxComponent?: any;
    labelComponent?: any;
    [key: string]: any;
};

const CheckboxGroup = ({
    name,
    label = '',
    id = name,
    checkboxComponent = undefined,
    labelComponent = undefined,
    ...props
}: CheckboxGroupProps) => {
    return (
        <div className='form-checkbox-group'>
            <Checkbox
                component={checkboxComponent}
                name={name}
                label={label}
                id={id}
                {...props}
            />
            <CheckboxLabel component={labelComponent} id={id} name={name}>
                {label}
            </CheckboxLabel>
        </div>
    );
};

export default CheckboxGroup;
