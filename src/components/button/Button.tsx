import React from 'react';
import { FormContext } from '../../context';
import { FormState } from '../../types';
import './Button.scss';

type ButtonType = 'button' | 'reset' | 'submit' | undefined;

type ButtonProps = {
    component: any;
    type: ButtonType;
    disabled: (form: FormState) => boolean;
    [key: string]: any;
};

const Button = ({
    children,
    type = 'submit',
    component: CustomButton,
    disabled = (form: FormState) => form.isSubmitting,
    ...props
}: React.PropsWithChildren<ButtonProps>) => {
    const [form] = React.useContext(FormContext);

    if (CustomButton) {
        return (
            <CustomButton type={type} disabled={disabled(form)} {...props}>
                {children}
            </CustomButton>
        );
    }

    return (
        <button
            type={type}
            disabled={disabled(form)}
            className={`button ${type}${disabled(form) ? ' disabled' : ''}`}
            {...props}
        >
            {children}
        </button>
    );
};

export default Button;
