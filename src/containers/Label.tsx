import React from 'react';
import './Label.scss';

const Label = ({
    children,
    name = undefined,
    ...props
}: React.PropsWithChildren<any>) => {
    return (
        <label {...props} htmlFor={name} className='form-label'>
            {children}
        </label>
    );
};

export default Label;
