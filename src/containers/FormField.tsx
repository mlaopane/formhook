import React from 'react';
import './FormField.scss';

const FormField = ({ children, ...props }: React.PropsWithChildren<any>) => {
    return (
        <div className='form-field' {...props}>
            {children}
        </div>
    );
};

export default FormField;
