import { Option, Document, ValidatorPool } from '../types';

type FormState = {
    values: Document<any>;
    errors: Document<string>;
    touched: Document<boolean>;
    validators: Document<ValidatorPool>;
    options: Document<[Option]>;
    focused: string;
    isSubmitting: boolean;
    isValid: boolean;
};

export default FormState;
