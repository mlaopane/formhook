import Field from './Field';
import FormState from './FormState';

type ValidationFunctionArgs = {
    field: Field;
    form: FormState;
};

export type AsyncValidationFunction = ({
    field,
    form,
}: ValidationFunctionArgs) => Promise<string>;

export type ValidatorPool = {
    validate?: AsyncValidationFunction;
    validateOnSubmit?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnBlur?: AsyncValidationFunction;
};
