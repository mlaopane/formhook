import {
    BLUR_FIELD,
    CHANGE_FIELD,
    CLEAR_FIELD_ERROR,
    END_SUBMIT,
    FOCUS_FIELD,
    SET_FIELD_ERROR,
    SET_FIELD_OPTIONS,
    SET_FIELD_VALIDATORS,
    START_SUBMIT,
} from '../reducers/actions';

export type ActionType =
    | typeof END_SUBMIT
    | typeof START_SUBMIT
    | typeof BLUR_FIELD
    | typeof CHANGE_FIELD
    | typeof CLEAR_FIELD_ERROR
    | typeof FOCUS_FIELD
    | typeof SET_FIELD_ERROR
    | typeof SET_FIELD_OPTIONS
    | typeof SET_FIELD_VALIDATORS;

export type Action = {
    type: ActionType;
    payload?: any;
};
