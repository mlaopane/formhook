import React from 'react';
import { Action } from './Action';

type Dispatch = React.Dispatch<Action>;

export default Dispatch;
