import React from 'react';
import { FormContext } from '../context';

function useError(name: string) {
    const [{ errors, focused }] = React.useContext(FormContext);

    return [errors[name], focused];
}

export default useError;
