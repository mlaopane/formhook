import React from 'react';
import { FormContext } from '../context';
import useValidator from './useValidator';
import useValidators from './useValidators';
import { AsyncValidationFunction } from '../types';

type UseInputArgs = {
    name: string;
    validate?: AsyncValidationFunction;
    validateOnSubmit?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnBlur?: AsyncValidationFunction;
};

function useInput({
    name,
    validate,
    validateOnSubmit,
    validateOnChange,
    validateOnBlur,
}: UseInputArgs) {
    const [form, dispatch] = React.useContext(FormContext) as any;
    const [processValidation] = useValidator();
    const value = form.values[name];
    useValidators({
        name,
        validate,
        validateOnSubmit,
        validateOnChange,
    });

    const handleChange = async ({ target: field }) => {
        dispatch({ type: 'CHANGE_FIELD', payload: { field } });

        const validationFn = validateOnChange || validate;

        if (validationFn) {
            processValidation({ field, validationFn });
        }
    };

    const handleFocus = async ({ target }) => {
        const { name } = target;

        dispatch({
            type: 'FOCUS_FIELD',
            payload: {
                field: { name },
            },
        });
    };

    const handleBlur = async ({ target: field }) => {
        dispatch({ type: 'BLUR_FIELD', payload: { field } });

        const validationFn = validateOnBlur || validate;

        if (validationFn) {
            processValidation({ field, validationFn });
        }
    };

    const validateField = ({ field }) => {
        if (validate) {
            processValidation({ field, validationFn: validate });
        }
    };

    return [value, handleChange, handleFocus, handleBlur, validateField];
}

export default useInput;
