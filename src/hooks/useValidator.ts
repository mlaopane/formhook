import React from 'react';
import { FormContext } from '../context';
import { AsyncValidationFunction, Field } from '../types';

function useValidator() {
    const [form, dispatch] = React.useContext(FormContext) as any;

    const processValidation = async ({
        field,
        validationFn,
    }: {
        field: Field;
        validationFn: AsyncValidationFunction;
    }) => {
        const error = await validationFn({ field, form });
        const { name } = field;

        if (error) {
            dispatch({
                type: 'SET_FIELD_ERROR',
                payload: {
                    field: { name },
                    error,
                },
            });
        } else {
            dispatch({
                type: 'CLEAR_FIELD_ERROR',
                payload: {
                    field: { name },
                },
            });
        }
    };

    return [processValidation];
}

export default useValidator;
