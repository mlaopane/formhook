import React from 'react';
import { FormContext } from '../context';
import useInput from './useInput';
import { AsyncValidationFunction, Option } from '../types';

type UseSelectArgs = {
    name: string;
    getInitialOptions: () => Promise<Option[]>;
    validate: AsyncValidationFunction | undefined;
    validateOnChange: AsyncValidationFunction | undefined;
    validateOnBlur: AsyncValidationFunction | undefined;
};

export default function useSelect({
    name,
    getInitialOptions,
    validate,
    validateOnChange,
    validateOnBlur,
}: UseSelectArgs) {
    const [form, dispatch] = React.useContext(FormContext);
    const [value, handleChange, handleFocus, handleBlur] = useInput({
        name,
        validate,
        validateOnChange,
        validateOnBlur,
    });
    const options = form.options[name];
    const [optionsInitialized, setOptionsInitialized] = React.useState(false);

    async function initializeOptions() {
        if (optionsInitialized) {
            return;
        }
        dispatch({
            type: 'SET_FIELD_OPTIONS',
            payload: {
                field: { name },
                options: await getInitialOptions(),
            },
        });
        setOptionsInitialized(true);
    }

    React.useEffect(() => {
        initializeOptions();
    }, []);

    return [options, value, handleChange, handleFocus, handleBlur];
}
