import React from 'react';
import { FormContext } from '../context';
import useInput from './useInput';
import useValidator from './useValidator';
import { AsyncValidationFunction } from '../types';

type UseRadioProps = {
    name: string;
    id: string;
    validate?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnSubmit?: AsyncValidationFunction;
};

export default function useRadio({
    name,
    id,
    validate = undefined,
    validateOnChange = undefined,
    validateOnSubmit = undefined,
}: UseRadioProps) {
    const [, dispatch] = React.useContext(FormContext);
    const [value] = useInput({
        name,
        validate,
        validateOnSubmit,
        validateOnChange,
    });
    const [processValidation] = useValidator();

    const validationFn = validateOnChange || validate;

    const selectRadio = (event) => {
        event.preventDefault();
        const field = { name, value: id };
        dispatch({ type: 'CHANGE_FIELD', payload: { field } });
        dispatch({
            type: 'FOCUS_FIELD',
            payload: {
                field: { name },
            },
        });

        if (validationFn) {
            processValidation({
                field: { name, value: id },
                validationFn,
            });
        }
    };

    return [value, selectRadio];
}
