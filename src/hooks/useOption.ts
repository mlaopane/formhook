import React from 'react';
import { FormContext } from '../context';
import { splice } from '../helpers/array.helper';
import { Option } from '../types';

function getUpdatedOptionValue({
    selectedOptions,
    option,
}: {
    selectedOptions: string[];
    option: Option;
}) {
    // Search for the option
    const optionIndex = selectedOptions.findIndex(
        (selectedOption) => selectedOption === option.value,
    );

    // Append the value in the options if it does not exist
    if (-1 === optionIndex) {
        return [...selectedOptions, option.value];
    }

    // Remove the value from the options if it does exist
    return splice(selectedOptions, optionIndex);
}

type UseOptionArgs = {
    name: string;
    option: Option;
    multiple: boolean;
    onSelect: React.MouseEventHandler | undefined;
};

export default function useOption({
    name,
    option,
    multiple = false,
    onSelect = undefined,
}: UseOptionArgs) {
    const [form, dispatch] = React.useContext(FormContext);

    const handleClick: React.MouseEventHandler = (event) => {
        onSelect && onSelect(event);

        const action = {
            type: 'CHANGE_FIELD',
        };

        if (!multiple) {
            dispatch({
                ...action,
                payload: {
                    field: {
                        name,
                        value: option.value,
                    },
                },
            });

            return;
        }

        dispatch({
            ...action,
            payload: {
                field: {
                    name,
                    value: getUpdatedOptionValue({
                        selectedOptions: form.values[name] as string[],
                        option,
                    }),
                },
            },
        });
    };

    return [handleClick];
}
