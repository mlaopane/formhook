import { FormDispatch, FormState } from 'types';
function mapFieldErrorPromise({
    form,
    dispatch,
}: {
    form: FormState;
    dispatch: FormDispatch;
}) {
    const { values, validators } = form;
    return async function(name) {
        const validator = validators[name];
        const validate =
            validator && (validator.validateOnSubmit || validator.validate);
        const value = values[name];

        if (validate) {
            const error = await validate({
                field: { name, value },
                form,
            });

            if (error) {
                dispatch({
                    type: 'SET_FIELD_ERROR',
                    payload: {
                        field: { name },
                        error,
                    },
                });
                return { field: { name }, error };
            }
        }

        dispatch({
            type: 'CLEAR_FIELD_ERROR',
            payload: {
                field: { name },
            },
        });

        return { field: { name }, error: '' };
    };
}

function useFormValidator({ form, dispatch }) {
    const validateForm = async () => {
        const { validators } = form;
        const fieldNames = Object.keys(validators);
        const promises = fieldNames.map(
            mapFieldErrorPromise({ form, dispatch }),
        );
        const errors = await Promise.all(promises);
        const toErrorsByFieldName = (result, { field, error }) => {
            return {
                ...result,
                [field.name]: error,
            };
        };
        return errors.reduce(toErrorsByFieldName, {});
    };

    return [validateForm];
}

export default useFormValidator;
