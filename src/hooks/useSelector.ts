import { FormState, Option } from 'types';
import React from 'react';
import { FormContext } from '../context';

type UseSelectorArgs = {
    name: string;
    multiple: boolean;
};

function getSelectedOption({
    form,
    name,
}: {
    form: FormState;
    name: string;
}): Option | undefined {
    const { options, values } = form;
    const fieldOptions = options[name];
    const selectedValue = values[name];

    return fieldOptions.find((option) => option.value === selectedValue);
}

function getSelectedOptions({
    form,
    name,
}: {
    form: FormState;
    name: string;
}): Option[] {
    const { options, values } = form;
    const fieldOptions = options[name];
    const selectedValues = values[name];

    return fieldOptions.filter((option) =>
        selectedValues.includes(option.value),
    );
}

export default function useSelector({
    name,
    multiple = false,
}: UseSelectorArgs) {
    const [form] = React.useContext(FormContext);
    const [isOpen, setIsOpen] = React.useState(false);
    const wrapperRef = React.useRef<HTMLDivElement>(null);

    const closeSelector = () => {
        setIsOpen(false);
    };

    const selectOption = () => {
        if (!multiple) {
            closeSelector();
        }
    };

    const toggleSelect = () => {
        setIsOpen(!isOpen);
    };

    const handleClickOutside = (event: MouseEvent) => {
        if (
            wrapperRef &&
            wrapperRef.current &&
            null !== event.target &&
            !wrapperRef.current.contains(event.target as Node)
        ) {
            closeSelector();
        }
    };

    React.useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    if (!multiple) {
        const selection = getSelectedOption({ form, name });
        return { selection, isOpen, wrapperRef, selectOption, toggleSelect };
    }

    const selection = getSelectedOptions({ form, name });

    return { selection, isOpen, wrapperRef, selectOption, toggleSelect };
}
