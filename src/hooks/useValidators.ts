import { useEffect, useContext } from 'react';
import { FormContext } from '../context';
import { AsyncValidationFunction } from '../types';

type Props = {
    name: string;
    validate?: AsyncValidationFunction;
    validateOnChange?: AsyncValidationFunction;
    validateOnBlur?: AsyncValidationFunction;
    validateOnSubmit?: AsyncValidationFunction;
};

export default function useValidators({
    name,
    validate,
    validateOnChange,
    validateOnBlur,
    validateOnSubmit,
}: Props) {
    const [, dispatch] = useContext(FormContext);

    useEffect(() => {
        dispatch({
            type: 'SET_FIELD_VALIDATORS',
            payload: {
                field: { name },
                validate,
                validateOnChange,
                validateOnBlur,
                validateOnSubmit,
            },
        });
    }, [name, validate, validateOnChange, validateOnBlur, validateOnSubmit]);
}
