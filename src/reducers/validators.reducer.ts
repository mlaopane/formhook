import { Document, Field } from '../types';

type State = {
    validators: Document;
};

type Payload = {
    field: Field;
    validate: any;
    validateOnChange: any;
    validateOnBlur: any;
    validateOnSubmit: any;
};

type Action = {
    type: string;
    payload: Payload;
};

type ActionArgs = {
    state: State;
    payload: Payload;
};

export function setFieldValidators({ state, payload }: ActionArgs) {
    const {
        field,
        validate,
        validateOnChange,
        validateOnBlur,
        validateOnSubmit,
    } = payload;

    return {
        ...state,
        validators: {
            ...state.validators,
            [field.name]: {
                validate,
                validateOnChange,
                validateOnBlur,
                validateOnSubmit,
            },
        },
    };
}
