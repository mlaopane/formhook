type State = {
    isSubmitting: boolean;
};

type Action = {
    type: string;
};

type ReducerArgs = {
    state: State;
    action: Action;
};

type ActionArgs = {
    state: State;
};

export function startSubmit({ state }: ActionArgs): State {
    return {
        ...state,
        isSubmitting: true,
    };
}

export function endSubmit({ state }: ActionArgs): State {
    return {
        ...state,
        isSubmitting: false,
    };
}
